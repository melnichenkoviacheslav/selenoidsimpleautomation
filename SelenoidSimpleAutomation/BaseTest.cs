﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;

namespace SelenoidSimpleAutomation
{
    public class BaseTest
    {
        protected IWebDriver Driver;

        [OneTimeSetUp]
        public void Init()
        {
            #region Selenoid_Setup_Here
            //var seleniumServer = "http://IP_HERE:4444/wd/hub";

            //var capabilities = new DesiredCapabilities();
            //capabilities.Platform = new Platform(PlatformType.Any);
            //capabilities.SetCapability(CapabilityType.BrowserName, "chrome");
            //capabilities.SetCapability(CapabilityType.Version, "81.0");

            ////Selenoid attribute samples
            //capabilities.SetCapability("enableVNC", true);
            //capabilities.SetCapability("enableVideo", false);
            //capabilities.SetCapability("screenResolution", "1920x1080x24");

            //Driver = new RemoteWebDriver(new Uri(seleniumServer), capabilities);
            #endregion

            Driver = new ChromeDriver();

            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            Driver.Manage().Window.Maximize();
        }

        [SetUp]
        public void Setup()
        {
            Driver.Url = "https://duckduckgo.com/";
        }

        [OneTimeTearDown]
        public void Dispose()
        {
            Driver.Dispose();
        }
    }
}