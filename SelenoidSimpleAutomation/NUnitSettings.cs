﻿using NUnit.Framework;

[assembly: LevelOfParallelism(8)]
[assembly: Parallelizable(ParallelScope.Fixtures)]