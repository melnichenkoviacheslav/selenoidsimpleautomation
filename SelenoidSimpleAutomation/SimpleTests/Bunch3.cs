using System.Linq;
using System.Threading;
using FluentAssertions;
using FluentAssertions.Execution;
using NUnit.Framework;
using OpenQA.Selenium;

namespace SelenoidSimpleAutomation.SimpleTests
{
    public class Bunch3 : BaseTest
    {
        [Test]
        public void Test([Values("cat", "dog", "mouse")] string expected)
        {
            //Arrange
            //Act
            Driver.FindElement(By.Id("search_form_input_homepage")).SendKeys(expected);
            Driver.FindElement(By.Id("search_button_homepage")).Click();

            var actualSearchResults = Driver
                .FindElements(By.CssSelector("div.results_links_deep"))
                .Select(el => el.Text)
                .ToList();

            Thread.Sleep(5000);

            //Assert
            using (new AssertionScope())
            {
                foreach (var actualSearchResult in actualSearchResults)
                {
                    actualSearchResult.ToLower().Should().Contain(expected);
                }
            }
        }
    }
}
